package msgraph

import (
	"errors"

	"github.com/microsoftgraph/msgraph-sdk-go/models/odataerrors"
	"go.uber.org/zap"
)

func LogODataError(log *zap.Logger, err error) {
	switch err.(type) {
	case *odataerrors.ODataError:
		oErr := err.(*odataerrors.ODataError)
		log = log.With(zap.Error(oErr))
		if terr := oErr.GetError(); terr != nil {
			log = log.With(
				zap.Stringp("Code", terr.GetCode()),
				zap.Stringp("Message", terr.GetMessage()),
				zap.Any("Details", terr.GetDetails()),
				zap.Any("Extended", terr.GetAdditionalData()),
				zap.Stringp("Target", terr.GetTarget()),
				zap.Any("InnerError", terr.GetInnererror()),
			)
		}

		log.With(zap.Error(errors.New("OData Deserialization Error")))
	default:
		log = log.With(zap.Error(err))
	}

	log.Error("Failed to query")
}
