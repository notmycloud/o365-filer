package app

import (
	"context"
	"fmt"

	kiota "github.com/microsoft/kiota-authentication-azure-go"
	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/notmycloud/o365-filer/app/msgraph"
	"gitlab.com/notmycloud/o365-filer/config"
	"gitlab.com/notmycloud/o365-filer/config/engines/simple"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

// https://learn.microsoft.com/en-us/graph/permissions-reference#user-permissions
var requiredScopes = []string{
	"User.Read",      // Allows users to sign-in to the app, and allows the app to read the profile of signed-in users. It also allows the app to read basic company information of signed-in users.
	"Mail.ReadWrite", // Allows the app to create, read, update, and delete mail in all mailboxes without a signed-in user. Does not include permission to send mail.
	"Mail.Send",      // Allows the app to send mail as any user without a signed-in user.
}

// Appcmd runs the root app command to filter messages
//
// https://github.com/microsoftgraph/msgraph-sdk-go
// https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Overview/appId/7b85fe2c-632b-43f6-ad7c-533589a779e9/isMSAApp~/false
func Appcmd(_ *cobra.Command, _ []string) error {
	client, appConfig, log, debugLog := appSetup()

	testAuth(client, log.Named("Authenticate"))

	if appConfig.Folders == nil {
		log.Fatal("No mailbox folders defined")
	}

	for name, folder := range appConfig.Folders {
		folder.Name = name
		if !folder.ValidateIDs(client, log.Named("Validate").Named("Folder")) {
			continue
		}
		folder.Handle(client, log.Named("Folder").Named(name), debugLog.Named(name), appConfig.QueryLimit,
			appConfig.Log.Path)
	}

	return nil
}

func appSetup() (*msgraphsdk.GraphServiceClient, *config.Config, *zap.Logger, *zap.Logger) {
	// Unmarshal Config
	appConfig := &config.Config{}
	if err := viper.Unmarshal(appConfig, viper.DecodeHook(simple.AgeDurationHookFunc())); err != nil {
		panic(fmt.Errorf("failed to parse config: %w", err))
	}

	if appConfig.DebugConfig {
		yamlData, err := yaml.Marshal(appConfig)
		if err != nil {
			panic(fmt.Errorf("failed to marshal config to YAML: %w", err))
		}

		fmt.Printf("--- YAML ---\n%s\n", yamlData)
	}

	log, err := config.NewZapLogger(&appConfig.Log, appConfig.GetAppName())
	if err != nil {
		panic(fmt.Errorf("failed to setup logging: %w", err))
	}

	logConfig := appConfig.Log
	logConfig.Debug = true
	debugLog, err := config.NewZapLogger(&logConfig, appConfig.GetAppName())
	if err != nil {
		log.With(zap.Error(err)).Warn("Failed to setup debug logger")
	}

	if !appConfig.Validate(log.Named("Validate").Named("Config")) {
		panic(fmt.Errorf("invalid configuration"))
	}
	log.Info("Configuration validated!")

	// Tenant and Client IDs from App Registration
	// https://learn.microsoft.com/en-us/graph/auth-register-app-v2
	// tenantID := "65b3ee68-c495-418b-92ae-82cc5c213c43"
	// clientID := "7b85fe2c-632b-43f6-ad7c-533589a779e9"

	log.Info("Creating client identity")
	cred, err := appConfig.GetCredentials()
	if err != nil {
		log.With(zap.Error(err)).Fatal("Failed to instantiate credentials")
	}

	// https://learn.microsoft.com/en-us/graph/permissions-reference#mail-permissions
	log.Info("Requesting Auth Scopes")
	// With interactive end user consent specify the needed scopes
	auth, err := kiota.NewAzureIdentityAuthenticationProviderWithScopes(cred, requiredScopes)
	if err != nil {
		log.With(zap.Error(err)).Fatal("Failed to setup Azure authentication")
	}

	// Get a Graph Service Client Adapter object
	log.Info("Creating MS Graph Adapter")
	adapter, err := msgraphsdk.NewGraphRequestAdapter(auth)
	if err != nil {
		log.With(zap.Error(err)).Fatal("Failed to create MS Graph Adapter")
	}

	log.Info("Creating Graph Client")
	return msgraphsdk.NewGraphServiceClient(adapter), appConfig, log, debugLog
}

func testAuth(client *msgraphsdk.GraphServiceClient, log *zap.Logger) {
	log.Info("Requesting Authorization")

	me, err := client.Me().Get(context.Background(), nil)
	if err != nil {
		msgraph.LogODataError(log, err)
		log.Fatal("Authorization Check Failed!")

		return
	}

	log.With(
		zap.Strings("Name", []string{*me.GetGivenName(), *me.GetSurname()}),
		zap.Stringp("Email", me.GetMail()),
	).Info("Proceeding with mailbox")
}
