package config

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	schemagen "github.com/invopop/jsonschema"
	"github.com/invopop/yaml"
	schemavalidate "github.com/qri-io/jsonschema"
	"github.com/spf13/viper"
	"gitlab.com/notmycloud/o365-filer/config/engines/simple"
)

func UnmarshalViper() (*Config, error) {
	appConfig := &Config{}
	if err := viper.Unmarshal(appConfig, viper.DecodeHook(simple.AgeDurationHookFunc())); err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	if appConfig.DebugConfig {
		yamlData, err := yaml.Marshal(appConfig)
		if err != nil {
			return nil, fmt.Errorf("failed to marshal config to YAML: %w", err)
		}

		fmt.Printf("--- YAML ---\n%s\n", yamlData)
	}

	return appConfig, nil
}

func ValidateSchema() error {
	log.Printf("Creating JSON Schema for the config...")
	r := new(schemagen.Reflector)
	// if err := r.AddGoComments("gitlab.com/notmycloud/o365-filer", "./"); err != nil {
	// 	// deal with error
	// 	return fmt.Errorf("failed to add field comments: %w", err)
	// }

	schemaGen := r.Reflect(&Config{})
	schema, err := json.MarshalIndent(schemaGen, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to Marshal Schema JSON: %w", err)
	}

	log.Printf("JSON Schema generated!\n")

	rs := &schemavalidate.Schema{}
	if err := json.Unmarshal(schema, rs); err != nil {
		return fmt.Errorf("failed to Unmarshal Schema: %w", err)
	}

	log.Printf("Reading configuration from disk.\n")
	configData, err := os.ReadFile(viper.ConfigFileUsed())
	if err != nil {
		return fmt.Errorf("failed to read config file [%s]: %w", viper.ConfigFileUsed(), err)
	}

	log.Printf("Converting config to JSON\n")
	configJSON, err := yaml.YAMLToJSON(configData)
	if err != nil {
		return fmt.Errorf("failed to convert config to JSON: %w", err)
	}

	log.Printf("Validating configuration...\n")
	keyErrors, err := rs.ValidateBytes(context.Background(), configJSON)
	if err != nil {
		return fmt.Errorf("failed to validate config: %w", err)
	}
	for i, keyError := range keyErrors {
		log.Printf("Key Error [%d]: %v\n", i, keyError)
	}
	if len(keyErrors) > 0 {
		return fmt.Errorf("configuration is invalid")
	}

	return nil
}
