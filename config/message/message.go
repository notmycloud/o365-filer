package message

import (
	"fmt"
	"time"

	graphmodels "github.com/microsoftgraph/msgraph-sdk-go/models"
)

func NewMessage(rawMessage graphmodels.Messageable) *Message {
	return &Message{
		id: *rawMessage.GetId(),
		From: &Contact{
			Address: *rawMessage.GetFrom().GetEmailAddress().GetAddress(),
			Name:    *rawMessage.GetFrom().GetEmailAddress().GetName(),
		},
		To:             TransposeAddresses(rawMessage.GetToRecipients()),
		CC:             TransposeAddresses(rawMessage.GetCcRecipients()),
		Subject:        *rawMessage.GetSubject(),
		Received:       rawMessage.GetReceivedDateTime(),
		HasAttachments: *rawMessage.GetHasAttachments(),
		IsRead:         *rawMessage.GetIsRead(),
		Flag:           rawMessage.GetFlag().GetFlagStatus().String(),
	}
}

func TransposeAddresses(recipients []graphmodels.Recipientable) []*Contact {
	var addresses []*Contact

	for _, contact := range recipients {
		address := &Contact{
			Name:    *contact.GetEmailAddress().GetName(),
			Address: *contact.GetEmailAddress().GetAddress(),
		}

		addresses = append(addresses, address)
	}

	return addresses
}

type Message struct {
	id             string
	Actioned       bool
	From           *Contact
	To             []*Contact
	CC             []*Contact `json:",omitempty"`
	toCC           []*Contact
	Subject        string
	Received       *time.Time
	HasAttachments bool
	IsRead         bool
	Flag           string
}

func (m *Message) GetID() string {
	return m.id
}

func (m *Message) GetToArray() []string {
	var sArray []string
	for _, contact := range m.To {
		sArray = append(sArray, contact.String())
	}

	return sArray
}

func (m *Message) GetCCArray() []string {
	var sArray []string
	for _, contact := range m.CC {
		sArray = append(sArray, contact.String())
	}

	return sArray
}

func (m *Message) GetToCCContacts() []*Contact {
	if len(m.toCC) != 0 {
		return m.toCC
	}

	m.toCC = append(m.To, m.CC...)
	return m.toCC
}

func (m *Message) GetType() interface{} {
	return &Message{}
}

type Contact struct {
	Address string
	Name    string
}

func (c *Contact) String() string {
	return fmt.Sprintf("%s <%s>", c.Name, c.Address)
}

func (c *Contact) GetType() interface{} {
	return &Contact{}
}
