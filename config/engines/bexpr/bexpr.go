package bexpr

import (
	"github.com/hashicorp/go-bexpr"
	"go.uber.org/zap"
)

const Algorithm = "BExpr"

func NewEvaluator() *Evaluator {
	return &Evaluator{}
}

type Evaluator struct {
	compiled map[string]*bexpr.Evaluator
	dataType interface{}
}

func (e *Evaluator) Validate(expression string, log *zap.Logger) bool {
	log.Debug("Validating BExpr Evaluator")
	if e == nil {
		log.Error("Evaluator undefined")
		return false
	}
	if expression == "" {
		log.Error("Expression undefined")
		return false
	}

	if e.compiled == nil {
		e.compiled = make(map[string]*bexpr.Evaluator)
	}

	_, ok := e.compiled[expression]
	if ok {
		log.With(zap.String("Expression", expression)).Warn("Duplicate Expression")
		return ok
	}

	eval, err := bexpr.CreateEvaluator(expression)
	if err != nil {
		log.With(
			zap.Error(err),
			zap.Any("DataType", e.dataType),
			zap.String("Expression", expression),
		).Error("Failed to compile expression")
		return false
	}

	e.compiled[expression] = eval

	return true
}

func (e *Evaluator) SetDataType(dataType interface{}) {
	e.dataType = dataType
}

func (e *Evaluator) Evaluate(datum interface{}, expression string, log *zap.Logger) (bool, bool) {
	// log.With(
	// 	zap.Any("Data", datum),
	// 	zap.String("Expression", expression),
	// ).Debug("Evaluating expression")

	eval, ok := e.compiled[expression]
	if !ok {
		log.With(zap.String("Expression", expression)).Error("Expression not found")
		return false, false
	}

	result, err := eval.Evaluate(datum)
	if err != nil {
		log.With(
			zap.Error(err),
			zap.String("Expression", expression),
		).Error("Failed to evaluate expression")
		return false, false
	}

	if !result {
		// log.With(
		// 	zap.String("Expression", expression),
		// 	zap.Any("Data", datum),
		// ).Debug("Didn't match")
		return false, true
	}

	return true, true
}
