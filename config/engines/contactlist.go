package engines

import (
	"fmt"

	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type contactList struct {
	// Any contact must match the defined rules.
	Any *andOr `json:",omitempty" jsonschema:"title=Any Contact,anyof_required=any,description=Any contact must match the defined rules."`
	// All contacts must match the defined rules.
	All *andOr `json:",omitempty" jsonschema:"title=All Contacts,anyof_required=all,description=All contacts must match the defined rules."`
	// None of the contacts must match the defined rules.
	None *andOr `json:",omitempty" jsonschema:"title=No Contacts,anyof_required=none,description=No contact can match teh defined rules."`
}

func (cl *contactList) Validate(log *zap.Logger) bool {
	log.Debug("Validating Contact List")
	if cl == nil {
		return true
	}
	ok := true

	if !cl.Any.Validate(log.Named("Any")) {
		ok = false
	}
	if !cl.All.Validate(log.Named("All")) {
		ok = false
	}
	if !cl.None.Validate(log.Named("None")) {
		ok = false
	}

	return ok
}

func (cl *contactList) setEvaluator(algo string) {
	if cl == nil {
		return
	}
	if cl.Any != nil {
		cl.Any.SetEvaluator(algo, &message.Contact{})
	}
	if cl.All != nil {
		cl.All.SetEvaluator(algo, &message.Contact{})
	}
	if cl.None != nil {
		cl.None.SetEvaluator(algo, &message.Contact{})
	}
}

func (cl *contactList) match(contacts []*message.Contact, log *zap.Logger, eval evaluator) (bool, bool) {
	var matched, ok bool

	// Any contact must match
	if cl.Any != nil && cl.Any.HasRules() {
		matched, ok = cl.evalAny(contacts, log.Named("Any"), eval)
		if !ok {
			return false, false
		}
		if !matched {
			return false, true
		}

		log.Named("Any").Debug("Matched!")
	}

	// All contacts must match
	if cl.All != nil && cl.All.HasRules() {
		matched, ok = cl.evalAll(contacts, log.Named("All"), eval)
		if !ok {
			return false, false
		}
		if !matched {
			return false, true
		}

		log.Named("All").Debug("Matched!")
	}

	// No contacts must match
	if cl.None != nil && cl.None.HasRules() {
		matched, ok = cl.evalNone(contacts, log.Named("None"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}

		log.Named("None").Debug("Matched!")
	}

	return true, true
}

// Any contact must match
func (cl *contactList) evalAny(contacts []*message.Contact, log *zap.Logger, eval evaluator) (bool,
	bool) {

	if len(contacts) == 0 {
		log.Debug("No contacts to match")
		return true, !cl.Any.FailNoContacts
	}

	for contactIndex, contact := range contacts {
		matched, ok := cl.Any.Evaluate(contact, log.Named(fmt.Sprintf("Contact%02d", contactIndex)).Named("Evaluate"),
			eval)
		if !ok {
			return false, false
		}

		if matched {
			return true, true
		}
	}

	return false, true
}

// All contacts must match
func (cl *contactList) evalAll(contacts []*message.Contact, log *zap.Logger, eval evaluator) (bool,
	bool) {

	if len(contacts) == 0 {
		log.Debug("No contacts to match")
		return true, !cl.All.FailNoContacts
	}

	for contactIndex, contact := range contacts {
		matched, ok := cl.All.Evaluate(contact, log.Named(fmt.Sprintf("Contact%02d", contactIndex)).Named("Evaluate"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}

	return true, true
}

// No contacts must match
func (cl *contactList) evalNone(contacts []*message.Contact, log *zap.Logger, eval evaluator) (bool,
	bool) {

	if len(contacts) == 0 {
		log.Debug("No contacts to match")
		return true, true
	}

	for contactIndex, contact := range contacts {
		matched, ok := cl.None.Evaluate(contact, log.Named(fmt.Sprintf("Contact%02d", contactIndex)).Named("Evaluate"), eval)
		if !ok {
			return false, false
		}

		if matched {
			return false, true
		}
	}

	return true, true
}
