package engines

import (
	"gitlab.com/notmycloud/o365-filer/config/engines/aexpr"
	"gitlab.com/notmycloud/o365-filer/config/engines/bexpr"
	"gitlab.com/notmycloud/o365-filer/config/engines/simple"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type Engines struct {
	// Simple Matching Engine.
	Simple *simple.Evaluator `json:",omitempty" jsonschema:"title=Simple Matching Engine,anyof_required=simple,description=A simple matching engine."`
	// BExpr uses the Hashicorp Bexpr engine.
	BExpr *Expression `json:",omitempty" jsonschema:"title=Hashicorp Bexpr Engine,anyof_required=bexpr,description=A more advanced matching engine using Hashicorp's BExpr library.'"`
	// AExpr uses the AntonMedv expression engine. This engine supports RegEx matching.
	AExpr *Expression `json:",omitempty" jsonschema:"title=Anton Medv Expression Engine,anyof_required=aexpr,description=A very advanced matching engine for complex rules using Anton Medv's Expression Engine.'"`
}

func (e *Engines) Validate(log *zap.Logger) bool {
	log.Debug("Validating Engines")
	if e == nil {
		return true
	}
	ok := true

	if !e.Simple.Validate(log.Named("Simple")) {
		ok = false
	}
	e.BExpr.setEvaluator(bexpr.Algorithm)
	if !e.BExpr.Validate(log.Named("BExpr")) {
		ok = false
	}
	e.AExpr.setEvaluator(aexpr.Algorithm)
	if !e.AExpr.Validate(log.Named("AExpr")) {
		ok = false
	}

	return ok
}

func (e *Engines) Match(message *message.Message, log *zap.Logger) (bool, bool) {
	var matched, ok bool

	if e.Simple != nil {
		matched, ok = e.Simple.Match(message, log.Named("Simple"))
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}

		log.Named("Simple").Debug("Matched!")
	}

	if e.BExpr != nil {
		matched, ok = e.BExpr.Match(message, log.Named("BExpr"), bexpr.NewEvaluator())
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}

		log.Named("BExpr").Debug("Matched!")
	}

	if e.AExpr != nil {
		matched, ok = e.AExpr.Match(message, log.Named("AExpr"), aexpr.NewEvaluator())
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}

		log.Named("AExpr").Debug("Matched!")
	}

	return true, true
}
