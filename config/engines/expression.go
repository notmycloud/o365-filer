package engines

import (
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

// Expression contains one or more go-bexpr expressions to match against the message. If multiple are set,
// they all must return true for a successful match.
type Expression struct {
	// Global rules that apply to the message as a whole.
	Global *andOr `json:",omitempty" jsonschema:"title=Global,anyof_required=global,description=Rules that apply to the message as a whole."`
	// From rules that apply to just the sender.
	From *andOr `json:",omitempty" jsonschema:"title=From Address,anyof_required=from,description=Rules that apply to the sender only."`
	// To rules that apply to just the recipients in the To field.
	To *contactList `json:",omitempty" jsonschema:"title=To Addresses,anyof_required=to,description=Rules that apply to the recipients in the To field only."`
	// CC rules that apply to just the recipients in the CC field.
	CC *contactList `json:",omitempty" jsonschema:"title=CC Addresses,anyof_required=cc,description=Rules that apply to the recipients in the CC field only."`
	// ToCC rules that apply to recipients in both the To and CC fields.
	ToCC *contactList `json:",omitempty" jsonschema:"title=To and CC Addresses,anyof_required=tocc,description=Rules that apply to the recipients in both the To and CC Fields."`
	// FromToCC rules that apply to addresses in all the contact fields.
	FromToCC *contactList `json:",omitempty" jsonschema:"title=From To and CC Addresses,anyof_required=fromtocc,Description=Rules that apply to the addresses in all the contact fields."`
}

func (e *Expression) Validate(log *zap.Logger) bool {
	log.Debug("Validating Expressions")
	if e == nil {
		return true
	}
	ok := true

	if !e.Global.Validate(log.Named("Global")) {
		ok = false
	}
	if !e.From.Validate(log.Named("From")) {
		ok = false
	}
	if !e.To.Validate(log.Named("To")) {
		ok = false
	}
	if !e.CC.Validate(log.Named("CC")) {
		ok = false
	}
	if !e.ToCC.Validate(log.Named("ToCC")) {
		ok = false
	}
	if !e.FromToCC.Validate(log.Named("FromToCC")) {
		ok = false
	}

	return ok
}

func (e *Expression) setEvaluator(algo string) {
	if e == nil {
		return
	}
	e.Global.SetEvaluator(algo, &message.Message{})
	e.From.SetEvaluator(algo, &message.Contact{})
	e.To.setEvaluator(algo)
	e.CC.setEvaluator(algo)
	e.ToCC.setEvaluator(algo)
	e.FromToCC.setEvaluator(algo)
}

func (e *Expression) Match(email *message.Message, log *zap.Logger, eval evaluator) (bool, bool) {
	var matched, ok bool

	if e.Global != nil {
		matched, ok = e.Global.Evaluate(email, log.Named("Global"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}
	if e.From != nil {
		matched, ok = e.From.Evaluate(email.From, log.Named("From"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}

	if e.To != nil {
		matched, ok = e.To.match(email.To, log.Named("To"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}

	if e.CC != nil {
		matched, ok = e.CC.match(email.CC, log.Named("CC"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}

	if e.ToCC != nil {
		matched, ok = e.ToCC.match(email.GetToCCContacts(), log.Named("ToCC"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}

	if e.FromToCC != nil {
		matched, ok = e.FromToCC.match(append(email.GetToCCContacts(), email.From), log.Named("FromToCC"), eval)
		if !ok {
			return false, false
		}

		if !matched {
			return false, true
		}
	}

	return true, true
}
