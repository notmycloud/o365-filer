package simple

import (
	"regexp"
	"time"

	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type Evaluator struct {
	// FromToCC matches against any address (To, CC, From).
	FromToCC *address `json:",omitempty" jsonschema:"title=From To and CC Addresses,anyof_required=fromtocc,description=Match against any contact address."`
	// From address criteria.
	From *address `json:",omitempty" jsonschema:"title=From Address,anyof_required=from,description=Match against the sender."`
	// To address criteria.
	To *address `json:",omitempty" jsonschema:"title=To Addresses,anyof_required=to,description=Match against the To recipients."`
	// CC address criteria.
	CC *address `json:",omitempty" jsonschema:"title=CC Addresses,anyof_required=cc,description=Match against the CC recipients."`
	// ToCC matches against both To and CC addresses.
	ToCC *address `json:",omitempty" jsonschema:"title=To or CC Addresses,anyof_required=tocc,description=Match against any recipient."`
	// Subject criteria in RegEx format.
	Subject   string `json:",omitempty" jsonschema:"title=Subject RegEx,anyof_required=subject,example=.*[Aa]lert.*,default=Unset,description=A Regex expression to match against the subject."`
	subjectRE *regexp.Regexp
	// HasAttachments matches if the message has attachments (True) or not (False). If unset,
	// attachments will not be checked.
	HasAttachments *bool `json:",omitempty" jsonschema:"title=Has Attachments,anyof_required=attachment,default=Unset,description=Specify if the message must have attachments (True) or no attachments (False). If unset, it will not filter upon attachment status."`
	// Age will match against messages that are at least this old. Uses w(eek), d(ay), h(our), m(inute), s(econd) format.
	Age *Age `json:",omitempty" jsonschema:"title=Minimum Age,anyof_required=age,example=30d,description=Messages must be at least this old. Uses w(eek), d(ay), h(our), m(inute), s(econd) format."`
}

func (e *Evaluator) Validate(log *zap.Logger) bool {
	log.Debug("Validating Simple Engine")
	if e == nil {
		return true
	}
	ok := true

	if !e.FromToCC.Validate(log.Named("FromToCC")) {
		ok = false
	}
	if !e.From.Validate(log.Named("From")) {
		ok = false
	}
	if !e.To.Validate(log.Named("To")) {
		ok = false
	}
	if !e.ToCC.Validate(log.Named("ToCC")) {
		ok = false
	}
	if e.Subject != "" {
		// https://github.com/google/re2/wiki/Syntax
		re, err := regexp.Compile(e.Subject)
		if err != nil {
			log.With(
				zap.Error(err),
				zap.String("Regex", e.Subject),
			).Error("Regex syntax invalid")
			ok = false
		}
		e.subjectRE = re
	}

	return ok
}

func (e *Evaluator) Match(message *message.Message, log *zap.Logger) (bool, bool) {
	var status status

	// if e.Name == "" || msg.From.Name == e.Name {
	// 	nameMatch = true
	// }
	status.Name = true

	status.AnyAddress = e.checkRecipients(
		log.Named("FromToCC"),
		append(message.GetToCCContacts(), message.From),
		e.FromToCC,
	)

	if e.From != nil {
		status.From = e.From.Match(log.Named("From"), message.From)
	} else {
		status.From = true
	}

	status.To = e.checkRecipients(log.Named("To"), message.To, e.To)

	status.CC = e.checkRecipients(log.Named("CC"), message.To, e.CC)

	status.ToCC = e.checkRecipients(log.Named("ToCC"), message.GetToCCContacts(), e.ToCC)

	if e.subjectRE == nil {
		status.Subject = true
	} else {
		// https://github.com/google/re2/wiki/Syntax
		status.Subject = e.subjectRE.Match([]byte(message.Subject))
	}

	if e.HasAttachments == nil || *e.HasAttachments == message.HasAttachments {
		status.Attachment = true
	}

	if message.Received.Before(time.Now().Add(-1 * e.Age.Duration())) {
		status.Age = true
	}

	return status.matched(log, e, message), true
}

func (e *Evaluator) checkRecipients(log *zap.Logger, recipients []*message.Contact, criteria *address) bool {
	if criteria == nil {
		log.Debug("Nil criteria defined")
		return true
	}
	if criteria.Address == "" && criteria.Domain == "" && criteria.Name == "" {
		// Return a true match if no conditions specified.
		log.Debug("No criteria defined")
		return true
	}

	for _, contact := range recipients {
		// Check address and Domain for match
		if criteria.Match(log, contact) {
			return true
		}
	}

	log.Debug("No Match")
	return false
}
