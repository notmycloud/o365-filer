package simple

import (
	"regexp"
	"strings"

	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type address struct {
	// Address must match, case-insensitive.
	Address string `json:",omitempty" jsonschema:"title=Address,anyof_required=address,example=bgates@microsoft.com,description=Case-insensitive match of the entire email address."`
	// Domain that the address must contain,
	// case-insensitive. Technically this just looks for a sub-string and could also be used to match the left side
	// of the @ symbol as well.
	Domain string `json:",omitempty" jsonschema:"title=Domain,anyof_required=domain,example=microsoft.com,description=Case-insensitive substring match, useful for matching the domain, but can be any substring."`
	// Name must match, case-insensitive.
	Name string `json:",omitempty" jsonschema:"title=Name,anyof_required=name,example=bill gates,description=Case-insensitive match of the contact name."`
	// Regex expression to match the address string. Format of "Name <email>"
	Regex string `json:",omitempty" jsonschema:"title=RegEx,anyof_required=regex,example=.*@.*microsoft.com,description=RegEx expression to match the address string in the format of 'Name <email>'."`
	regex *regexp.Regexp
}

func (a *address) Validate(log *zap.Logger) bool {
	log.Debug("Validating Address")
	if a == nil {
		return true
	}
	ok := true

	if a.Regex != "" {
		// https://github.com/google/re2/wiki/Syntax
		re, err := regexp.Compile(a.Regex)
		if err != nil {
			log.With(
				zap.Error(err),
				zap.String("Regex", a.Regex),
			).Error("Regex syntax invalid")
			ok = false
		}
		a.regex = re
	}

	return ok
}

func (a *address) Match(log *zap.Logger, address *message.Contact) bool {

	if a.Address == "" && a.Domain == "" && a.Name == "" && a.Regex == "" {
		log.Debug("No match criteria")
		return true
	}
	log.With(
		zap.Any("Criteria", a),
		zap.String("address", address.String()),
	).Debug("Checking criteria")

	if a.Address != "" && strings.ToLower(address.Address) == strings.ToLower(a.Address) {
		log.Debug("address Matches")
	} else {
		return false
	}

	if a.Domain != "" && strings.Contains(strings.ToLower(address.Address), strings.ToLower(a.Domain)) {
		log.Debug("Domain Matches")
	} else {
		return false
	}

	if a.Name != "" && strings.ToLower(address.Name) == strings.ToLower(a.Name) {
		log.Debug("Name Matches")
	} else {
		return false
	}

	if a.regex != nil {
		// https://github.com/google/re2/wiki/Syntax
		if a.regex.Match([]byte(address.String())) {
			log.With(zap.String("Regex", a.Regex)).Debug("Regex Matches")
		} else {
			return false
		}
	}

	return true
}
