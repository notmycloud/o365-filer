package simple

import (
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type status struct {
	Name       bool
	AnyAddress bool
	From       bool
	To         bool
	CC         bool
	ToCC       bool
	Subject    bool
	Attachment bool
	Age        bool
}

func (s status) matched(log *zap.Logger, match *Evaluator, message *message.Message) bool {
	matched := true

	if !s.Name {
		log = log.With(zap.String("Name", "Not Implemented"))
		matched = false
	}

	if !s.AnyAddress {
		log = log.With(
			zap.Any("Criteria", match.FromToCC),
			zap.String("From Actual", message.From.String()),
			zap.Strings("To Actual", message.GetToArray()),
			zap.Strings("CC Actual", message.GetCCArray()),
		)
		matched = false
	}

	if !s.From {
		log = log.With(
			zap.Any("From Criteria", match.From),
			zap.String("From Actual", message.From.String()),
		)
		matched = false
	}

	if !s.To {
		log = log.With(
			zap.Any("To Criteria", match.To),
			zap.Strings("To Actual", message.GetToArray()),
		)
		matched = false
	}

	if !s.CC {
		log = log.With(
			zap.Any("CC Criteria", match.To),
			zap.Strings("CC Actual", message.GetCCArray()),
		)
		matched = false
	}

	if !s.ToCC {
		log = log.With(
			zap.Any("Criteria", match.ToCC),
			zap.Strings("To Actual", message.GetToArray()),
			zap.Strings("CC Actual", message.GetCCArray()),
		)
		matched = false
	}

	if !s.Subject {
		log = log.With(
			zap.String("Subject Criteria", match.Subject),
			zap.String("Subject Actual", message.Subject),
		)
		matched = false
	}

	if !s.Attachment {
		log = log.With(
			zap.Boolp("Attachment Criteria", match.HasAttachments),
			zap.Bool("Attachment Actual", message.HasAttachments),
		)
		matched = false
	}

	if !s.Age {
		log = log.With(
			zap.Duration("Date Criteria", match.Age.Duration()),
			zap.Timep("Date Actual", message.Received),
		)
		matched = false
	}

	if matched {
		log.Debug("Message Matched!")
		return matched
	}

	log.Debug("Message Not Matched")
	return matched
}
