package simple

import (
	"fmt"
	"reflect"
	"time"

	"github.com/invopop/jsonschema"
	"github.com/mitchellh/mapstructure"
	"github.com/xhit/go-str2duration/v2"
)

type Age time.Duration

func (a Age) Duration() time.Duration {
	return time.Duration(a)
}

func AgeDurationHookFunc() mapstructure.DecodeHookFuncType {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{},
	) (interface{}, error) {
		// Check that the data is string
		if f.Kind() != reflect.String {
			return data, nil
		}

		testAge, err := time.ParseDuration("1s")
		if err != nil {
			return nil, fmt.Errorf("failed to parse sample duration: %w", err)
		}
		// Check that the target type is our custom type
		if t.Kind() != reflect.TypeOf(testAge).Kind() {
			return data, nil
		}

		// Parse the duration with days and weeks
		ageDuration, err := str2duration.ParseDuration(data.(string))
		if err != nil {
			return nil, fmt.Errorf("failed to parse duration [%v]: %s", data, err)
		}

		// Return the parsed value
		return ageDuration, nil
	}
}

func (a Age) JSONSchema() *jsonschema.Schema {
	return &jsonschema.Schema{
		Type:        "string",
		Title:       "Age",
		Description: "Shorthand age definition, Uses w(eek), d(ay), h(our), m(inute), s(econd) format.",
		Pattern:     "^[0-9]*[wdhms]$",
		Examples: []interface{}{
			"4w",
			"30d",
			"12h",
		},
	}
}
