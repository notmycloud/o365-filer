package aexpr

import (
	"github.com/antonmedv/expr"
	"github.com/antonmedv/expr/vm"
	"go.uber.org/zap"
)

const Algorithm = "AExpr"

func NewEvaluator() *Evaluator {
	return &Evaluator{}
}

type Evaluator struct {
	compiled map[string]*vm.Program
	dataType interface{}
}

func (e *Evaluator) Validate(expression string, log *zap.Logger) bool {
	log.Debug("Validating AExpr Evaluator")
	if e == nil {
		log.Error("Evaluator undefined")
		return false
	}
	if expression == "" {
		log.Error("Expression undefined")
		return false
	}

	if e.compiled == nil {
		e.compiled = make(map[string]*vm.Program)
	}

	_, ok := e.compiled[expression]
	if ok {
		log.With(zap.String("Expression", expression)).Warn("Duplicate Expression")
		return ok
	}

	eval, err := expr.Compile(expression, expr.Env(e.dataType), expr.AsBool())
	if err != nil {
		log.With(
			zap.Error(err),
			zap.Any("DataType", e.dataType),
			zap.String("Expression", expression),
		).Error("Failed to compile expression")
		return false
	}

	e.compiled[expression] = eval

	return true
}

func (e *Evaluator) SetDataType(dataType interface{}) {
	e.dataType = dataType
}

func (e *Evaluator) Evaluate(datum interface{}, expression string, log *zap.Logger) (bool, bool) {
	// log.With(
	// 	zap.Any("Data", datum),
	// 	zap.String("Expression", expression),
	// ).Debug("Evaluating expression")

	eval, ok := e.compiled[expression]
	if !ok {
		log.With(zap.String("Expression", expression)).Error("Expression not found")
		return false, false
	}

	result, err := expr.Run(eval, datum)
	if err != nil {
		log.With(
			zap.Error(err),
			zap.String("Expression", expression),
		).Error("Failed to evaluate expression")
		return false, false
	}

	matched, ok := result.(bool)
	if !ok {
		log.Error("Failed to evaluate expression result")
		return false, false
	}

	if !matched {
		// log.With(
		// 	zap.String("Expression", expression),
		// 	zap.Any("Data", datum),
		// ).Debug("Didn't match")
		return false, true
	}

	return true, true
}
