package engines

import (
	"fmt"

	"gitlab.com/notmycloud/o365-filer/config/engines/aexpr"
	"gitlab.com/notmycloud/o365-filer/config/engines/bexpr"
	"go.uber.org/zap"
)

type evaluator interface {
	Evaluate(interface{}, string, *zap.Logger) (bool, bool)
	Validate(string, *zap.Logger) bool
	SetDataType(interface{})
}

type andOr struct {
	// And contains rules that must all match.
	And []string `json:",omitempty" jsonschema:"title=All Rules,anyof_required=And,description=All specified rules must match."`
	// Or contains rules that must have at least one match.
	Or        []string `json:",omitempty" jsonschema:"title=Any Rules,anyof_required=Or,description=At least one rule must match."`
	evaluator evaluator
	// FailNoContacts will fail if the contact list is empty
	FailNoContacts bool `json:",omitempty" jsonschema:"title=Fail Empty Contact List,default=False,description=Fail this ruleset if no contacts are defined in this contact list."`
}

func (ao *andOr) Validate(log *zap.Logger) bool {
	log.Debug("Validating Constructor")
	if ao == nil {
		return true
	}
	if ao.evaluator == nil {
		log.Error("Evaluator undefined")
		return false
	}
	ok := true

	for _, expr := range ao.And {
		if !ao.evaluator.Validate(expr, log.Named("Evaluator").With(zap.String("Expression", expr))) {
			ok = false
		}
	}
	for _, expr := range ao.Or {
		if !ao.evaluator.Validate(expr, log.Named("Evaluator").With(zap.String("Expression", expr))) {
			ok = false
		}
	}

	return ok
}

func (ao *andOr) SetEvaluator(algo string, dataType interface{}) {
	if ao == nil {
		return
	}
	switch algo {
	case bexpr.Algorithm:
		ao.evaluator = bexpr.NewEvaluator()
	case aexpr.Algorithm:
		ao.evaluator = aexpr.NewEvaluator()
	}
	ao.evaluator.SetDataType(dataType)
}

func (ao *andOr) HasRules() bool {
	if len(ao.And) == 0 && len(ao.Or) == 0 {
		return false
	}

	return true
}

func (ao *andOr) Evaluate(datum interface{}, log *zap.Logger, eval evaluator) (bool, bool) {
	if ao.evaluator == nil {
		ao.evaluator = eval
	}

	// All expressions must match
	for i, expr := range ao.And {
		matched, ok := ao.evaluator.Evaluate(datum, expr, log.Named("And").Named(fmt.Sprintf("Expression%02d", i)))
		if !ok {
			return false, false
		}

		log.Named(fmt.Sprintf("And.Expression%02d", i)).With(
			zap.Bool("Matched", matched),
			zap.String("Expression", expr),
		).Debug("Result")

		if !matched {
			return false, true
		}
	}

	if len(ao.Or) == 0 {
		return true, true
	}

	// At least one expression must match
	for i, expr := range ao.Or {
		matched, ok := ao.evaluator.Evaluate(datum, expr, log.Named("Or").Named(fmt.Sprintf("Expression%02d", i)))
		if !ok {
			return false, false
		}

		log.Named(fmt.Sprintf("Or.Expression%02d", i)).With(
			zap.Bool("Matched", matched),
			zap.String("Expression", expr),
			zap.Any("Data", datum),
		).Debug("Result")

		if matched {
			return true, true
		}
	}

	return false, true
}
