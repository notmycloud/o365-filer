package config

import (
	"path/filepath"
)

type logging struct { // TODO: Require Path if FileJSON or Rotate is set. https://github.com/invopop/jsonschema/issues/60
	// Development enables stack traces on ERROR level.
	Development bool `json:",omitempty" jsonschema:"title=Development Logging,default=False,description=Enables stack traces on ERROR level logs, only for development use."`
	// Path to write logs to.
	Path string `json:",omitempty" jsonschema:"title=Logging Path,example=./logs,default=Current Directory,description=Optional path to create the log files, required if FileJSON or Rotate options are set."`
	// DEBUG level logging.
	Debug bool `json:",omitempty" jsonschema:"title=Debug Logging,default=False,description=Enable DEBUG level log messages."`
	// ConsoleJSON sets the Console output to JSON format.
	ConsoleJSON bool `json:",omitempty" jsonschema:"title=Console JSON Format,default=False,description=Format the Console log messages in JSON."`
	// FileJSON sets the File output to JSON format.
	FileJSON bool `json:",omitempty" jsonschema:"title=File JSON Format,default=False,description=Format the file log messages in JSON."`
	// RotateEnable will rotate the log files based on the criteria specified.
	Rotate   *rotate `json:",omitempty" jsonschema:"title=Rotate Logs,description=Configure the criteria for rotating the log file. If this key does not exist, the log will not rotate."`
	filePath string
	name     string
}

func (l *logging) GetLogFilePath() string {
	if l.filePath == "" {
		l.filePath = filepath.Join(l.Path, l.name+".log")
	}

	return l.filePath
}
