package config

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"gitlab.com/notmycloud/o365-filer/config/api"
	"go.uber.org/zap"
)

type authMethod string

const (
	AuthDeviceCode authMethod = "DeviceCode"
	AuthBrowser    authMethod = "Browser"
)

type Config struct {
	// TenantID is the ID for your Microsoft 365 Tenant.
	TenantID string `jsonschema:"title=Microsoft 365 Tenant ID,required,example=abcd1234-def0-1234-abcd-abcdef123456,description=Your Microsoft 365 Tenant ID."`
	// ClientID is your application specific ClientID credential.
	ClientID string `jsonschema:"title=App registration Client ID,required,example=abcdef12-3456-7890-abcd-abcdef123456,description=The Client ID given in your App Registration."`
	// AuthMethod determines which method you prefer to authenticate this application with Microsoft 365.
	AuthMethod authMethod `jsonschema:"title=Authentication Method,example=DeviceCode,default=Browser,enum=DeviceCode,enum=Browser,description=Specify your preferred method to authenticate with Microsoft 365."`
	// Log configuration options
	Log logging `json:",omitempty" jsonschema:"title=Logging Options,description=Configuration options for the application logger."`
	// Folders contains the mailbox folder hierarchy to operate upon. Folders will be processed in alphanumeric order.
	Folders map[string]*api.Folder `jsonschema:"title=Mail Folders,required,example=Inbox,description=Named folders hierarchy to process in alphanumeric order."`
	appName string
	// DebugConfig enables a dump of the combined configuration.
	DebugConfig bool `json:",omitempty" jsonschema:"title=Debug Config,default=False,description=Dump the operating configuration upon load."`
	// QueryLimit limits how many messages to fetch from the API.
	QueryLimit *int32 `json:",omitempty" jsonschema:"title=Query Limit,example=100,default=Unlimited,description=Limit the number of messages to fetch from the API."`
}

func (c *Config) Validate(log *zap.Logger) bool {
	log.Debug("Validating Config")
	if c == nil {
		return true
	}
	ok := true

	if c.TenantID == "" {
		log.Named("TenantID").Error("Undefined")
		ok = false
	}
	if c.ClientID == "" {
		log.Named("ClientID").Error("Undefined")
		ok = false
	}
	if c.AuthMethod == "" {
		log.Named("AuthMethod").With(zap.String("Method", string(AuthDeviceCode))).Warn("Undefined, using default")
		c.AuthMethod = AuthDeviceCode
	}
	if c.Folders == nil {
		log.Named("Folders").Error("Undefined")
		ok = false
	} else {
		for name, folder := range c.Folders {
			folder.Name = name
			if !folder.Validate(log.Named("Folder").With(zap.String("Folder", folder.Name))) {
				ok = false
			}
		}
	}

	return ok
}

func (c *Config) GetAppName() string {
	if c.appName == "" {
		c.appName = strings.TrimSuffix(strings.ToUpper(filepath.Base(os.Args[0])), ".EXE")
		c.Log.name = c.appName
	}

	return c.appName
}

func (c *Config) GetCredentials() (azcore.TokenCredential, error) {
	switch c.AuthMethod {
	case AuthDeviceCode:
		return c.getDeviceCodeCred()
	case AuthBrowser:
		return c.getInteractiveBrowserCred()
	default:
		return nil, errors.New("invalid Authentication Method")
	}
}

func (c *Config) getDeviceCodeCred() (*azidentity.DeviceCodeCredential, error) {
	cred, err := azidentity.NewDeviceCodeCredential(&azidentity.DeviceCodeCredentialOptions{
		TenantID: c.TenantID,
		ClientID: c.ClientID,
		UserPrompt: func(ctx context.Context, message azidentity.DeviceCodeMessage) error {
			log.Println(message.Message)
			return nil
		},
	})

	if err != nil {
		return nil, fmt.Errorf("failed to get Device Code Credential: %w", err)
	}

	return cred, nil
}

func (c *Config) getInteractiveBrowserCred() (*azidentity.InteractiveBrowserCredential, error) {

	cred, err := azidentity.NewInteractiveBrowserCredential(&azidentity.InteractiveBrowserCredentialOptions{
		TenantID:    c.TenantID,
		ClientID:    c.ClientID,
		RedirectURL: "http://localhost:54321",
	})

	if err != nil {
		return nil, fmt.Errorf("failed to get Interactive Browser Credential: %w", err)
	}

	return cred, nil
}
