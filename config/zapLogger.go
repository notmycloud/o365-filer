package config

import (
	"fmt"
	"log"
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewZapLogger(options *logging, logName string) (*zap.Logger, error) {
	logConfig, err := configureLogConfig(options)
	if err != nil {
		return nil, fmt.Errorf("could not configure logger config: %w", err)
	}

	logLevel, err := configureLogLevel(options)
	if err != nil { // Soft fail with default log level
		log.Printf("Failed to configure Log Level, falling back to default: %v\n", err)
	}

	logCoreConsole, err := generateLogCoreConsole(options, logConfig, logLevel)
	if err != nil {
		return nil, fmt.Errorf("could not setup console logging: %w", err)
	}

	logCore := logCoreConsole

	logCoreFile, err := generateLogCoreFile(options, logConfig, logLevel)
	if err != nil {
		return nil, fmt.Errorf("could not setup file logging: %w", err)
	}

	if logCoreFile != nil {
		logCore = zapcore.NewTee(
			logCore,
			logCoreFile,
		)
	}

	return zap.New(logCore, zap.AddStacktrace(zapcore.ErrorLevel), zap.AddCaller()).Named(logName), nil
}

func configureLogConfig(options *logging) (zapcore.EncoderConfig, error) {
	if options.Development {
		config := zap.NewDevelopmentEncoderConfig()
		config.EncodeTime = zapcore.ISO8601TimeEncoder

		return config, nil
	}

	logConfig := zap.NewProductionEncoderConfig()
	logConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	return logConfig, nil
}

func configureLogLevel(options *logging) (zapcore.Level, error) {
	if options.Debug {
		return zap.DebugLevel, nil
	}

	return zap.InfoLevel, nil
}

func generateLogCoreConsole( //nolint:ireturn
	options *logging,
	logConfig zapcore.EncoderConfig,
	logLevel zapcore.Level,
) (zapcore.Core, error) {
	logEncoder, err := configureLogEncoder(options.ConsoleJSON, logConfig)
	if err != nil {
		return nil, fmt.Errorf("could not configure Console Log Encoder: %w", err)
	}

	return zapcore.NewCore(logEncoder, zapcore.AddSync(os.Stdout), logLevel), nil
}

func generateLogCoreFile( //nolint:ireturn
	options *logging,
	logConfig zapcore.EncoderConfig,
	logLevel zapcore.Level,
) (zapcore.Core, error) {
	logEncoder, err := configureLogEncoder(options.FileJSON, logConfig)
	if err != nil {
		return nil, fmt.Errorf("could not configure File Log Encoder: %w", err)
	}

	logWriter, err := configureLogFileWriter(options)
	if err != nil {
		return nil, fmt.Errorf("could not configure File Log Writer: %w", err)
	}

	// If no logWriter was returned and we have no error, then File Logging is disabled.
	if logWriter == nil {
		return nil, nil
	}

	return zapcore.NewCore(logEncoder, logWriter, logLevel), nil
}

func configureLogEncoder( //nolint:ireturn
	JSON bool,
	zapConfig zapcore.EncoderConfig,
) (zapcore.Encoder, error) {
	if JSON {
		return zapcore.NewJSONEncoder(zapConfig), nil
	}

	return zapcore.NewConsoleEncoder(zapConfig), nil
}

func configureLogFileWriter(options *logging) (zapcore.WriteSyncer, error) { //nolint:ireturn
	if options.Path == "" {
		return nil, nil
	}

	if options.Rotate != nil {
		return options.Rotate.rotateWriter(options.GetLogFilePath()), nil
	}

	logFile, err := os.OpenFile(
		options.GetLogFilePath(),
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0o644, //nolint:gomnd
	)
	if err != nil {
		return nil, fmt.Errorf("could not open log file [%s] Error: %w", options.GetLogFilePath(), err)
	}

	return zapcore.AddSync(logFile), nil
}
