package api

import (
	"fmt"
	"sort"
	"strings"

	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type Category struct {
	name string
	// Disable will prevent the rules in this category from executing.
	Disable bool `json:",omitempty" jsonschema:"title=Disable Rules,default=False,description=Prevent the rules in this category from processing."`
	// Rules to apply to the messages in the specified folder.
	Rules map[string]Rule `jsonschema:"title=Category Rules,required,description=Rules to apply to messages in this folder."`
}

func (c *Category) Validate(log *zap.Logger) bool {
	log.Debug("Validating Category")
	if c == nil {
		return true
	}
	ok := true

	if c.Disable {
		log.Info("Disabled")
		return true
	}

	if c.Rules == nil {
		log.Named("Rules").Error("Undefined")
	} else {
		for name, rule := range c.Rules {
			rule.name = name
			if !rule.Validate(log.Named("Rule").With(zap.String("Rule", rule.name))) {
				ok = false
			}
		}
	}

	return ok
}

func (c *Category) runRules(client *msgraphsdk.GraphServiceClient,
	log *zap.Logger, emails []*message.Message, debugLog *zap.Logger) {
	if c.Rules == nil {
		log.Error("No rules defined!")
		return
	}

	log.With(zap.Int("RuleCount", len(c.Rules)), zap.Int("MessageCount",
		len(emails))).Info("Processing Rules")

	log = log.Named("Message")
	for msgIndex, msg := range emails {
		msgLog := log.Named(fmt.Sprint(msgIndex))
		if msg.Actioned {
			msgLog.With(
				zap.String("Subject", msg.Subject),
				zap.Timep("Received", msg.Received),
				zap.String("From", msg.From.Address),
				zap.Strings("To", msg.GetToArray()),
				zap.Strings("CC", msg.GetCCArray()),
				zap.Bool("Attachments", msg.HasAttachments),
				zap.Bool("Read", msg.IsRead),
				zap.String("Flag", msg.Flag),
			).Debug("Email already handled.")
			continue
		}

		msgLog.With(
			zap.String("Subject", msg.Subject),
			zap.Timep("Received", msg.Received),
			zap.String("From", msg.From.Address),
			zap.Strings("To", msg.GetToArray()),
			zap.Strings("CC", msg.GetCCArray()),
			zap.Bool("Attachments", msg.HasAttachments),
			zap.Bool("Read", msg.IsRead),
			zap.String("Flag", msg.Flag),
		).Info("Processing")

		// Get keys from the map so we can call categories in alphanumeric order
		keys := make([]string, 0, len(c.Rules))
		for k := range c.Rules {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		var matched bool
		var err error
		for _, ruleName := range keys {
			rule := c.Rules[ruleName]
			var ruleLog *zap.Logger
			if rule.Debug {
				ruleLog = debugLog.Named(fmt.Sprintf("Message%04d", msgIndex))
			} else {
				ruleLog = msgLog
			}

			if rule.Disable {
				ruleLog.Warn("Rule Disabled")
				continue
			}
			rule.name = strings.Join([]string{c.name, "Message", fmt.Sprint(msgIndex), ruleName}, ".")

			matched, err = rule.Process(client, msg, ruleLog.Named("Rule").Named(ruleName))
			if err != nil {
				ruleLog.With(zap.Error(err)).Error("Failed to process rule, quitting early.")
				break
			}

			if matched {
				emails[msgIndex].Actioned = true
				break
			}
		}

		if !matched {
			msgLog.Debug("Not matched.")
		}
	}
}
