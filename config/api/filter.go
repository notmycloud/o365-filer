package api

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/notmycloud/o365-filer/config/engines/simple"
)

type Filter struct {
	// Age specifies the minimum age of messages to be returned. Uses w(eek), d(ay), h(our), m(inute), s(econd) format.
	Age *simple.Age `json:",omitempty" jsonschema:"title=Message Age,anyof_required=age,example=30d,description=Specify the minimum age of messages to be returned. Uses w(eek), d(ay), h(our), m(inute), s(econd) format."`
	// Read specifies if messages must be read (True) or unread (False). If unset, it will not filter on read status.
	Read *bool `json:",omitempty" jsonschema:"title=Is Read,anyof_required=read,description=Specify if the message must be read (True) or unread (False). If unset, it will not filter upon read status."`
	// Flagged specifies if messages must be flagged (True) or not flagged (False). If unset,
	// it will not filter on flag status.
	Flagged *bool `json:",omitempty" jsonschema:"title=Is Flagged,anyof_required=flag,description=Specify if the message must be flagged (True) or not flagged (False). If unset, it will not filter upon flag status."`
	// Custom allows you to specify your own custom filter for options not specified in our configuration.
	// The custom string will be appended with an 'AND' to any of the specified options.
	Custom string `json:",omitempty" jsonschema:"title=Custom Options,anyof_required=custom,example=https://learn.microsoft.com/en-us/graph/filter-query-parameter,default=NONE,description=Custom allows you to specify your own custom filter for options not specified in our configuration. The custom string will be appended with an 'AND' to any of the specified options."`
}

func (f Filter) buildFilter() string {
	// "receivedDateTime le %v AND isREAD eq true AND flag/flagStatus ne 'flagged'",
	//		time.Now().AddDate(0, 0, -30).Format("2006-01-02")
	filterQuery := strings.Builder{}
	previousFilter := false

	if f.Age != nil {
		filterQuery.WriteString(fmt.Sprintf("receivedDateTime le %v",
			time.Now().Add(f.Age.Duration()*-1).Format("2006-01-02")))
		previousFilter = true
	}

	if f.Read != nil {
		if previousFilter {
			filterQuery.WriteString(" AND ")
		}
		filterQuery.WriteString(fmt.Sprintf("isREAD eq %t", f.Read))
		previousFilter = true
	}

	if f.Flagged != nil {
		if previousFilter {
			filterQuery.WriteString(" AND ")
		}
		if *f.Flagged {
			filterQuery.WriteString("flag/flagStatus eq 'flagged'")
		} else {
			filterQuery.WriteString("flag/flagStatus ne 'flagged'")
		}

		previousFilter = true
	}

	if f.Custom != "" {
		if previousFilter {
			filterQuery.WriteString(" AND ")
		}

		filterQuery.WriteString(f.Custom)

		previousFilter = true
	}
	return filterQuery.String()
}
