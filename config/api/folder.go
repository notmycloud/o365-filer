package api

import (
	"context"
	"encoding/json"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync/atomic"

	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	msgraphcore "github.com/microsoftgraph/msgraph-sdk-go-core"
	"github.com/microsoftgraph/msgraph-sdk-go/me/mailfolders/item/messages"
	graphmodels "github.com/microsoftgraph/msgraph-sdk-go/models"
	"github.com/schollz/progressbar/v3"
	"gitlab.com/notmycloud/o365-filer/app/msgraph"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

// Well Known Folders:
// https://learn.microsoft.com/en-us/graph/api/resources/mailfolder?view=graph-rest-1.0#:~:text=a%20delta%20function.-,Well%2Dknown%20folder%20names,-Outlook%20creates%20certain
//
// type wellKnownFolder string
//
// func (wkf wellKnownFolder) String() string {
// 	return string(wkf)
// }
//
// const (
// 	// The archive folder messages are sent to when using the One_Click Archive feature in
// 	// Outlook clients that support it. Note: this is not the same as the Archive Mailbox feature of Exchange online.
// 	FolderArchive wellKnownFolder = "archive"
// 	// The clutter folder low-priority messages are moved to when using the Clutter feature.
// 	FolderClutter wellKnownFolder = "clutter"
// 	// The folder that contains conflicting items in the mailbox.
// 	FolderConflicts wellKnownFolder = "conflicts"
// 	// The folder where Skype saves IM conversations (if Skype is configured to do so).
// 	FolderConversationHistory wellKnownFolder = "conversationhistory"
// 	// The folder items are moved to when they are deleted.
// 	FolderDeletedItems wellKnownFolder = "deleteditems"
// 	// The folder that contains unsent messages.
// 	FolderDrafts wellKnownFolder = "drafts"
// 	// The inbox folder.
// 	FolderInbox wellKnownFolder = "inbox"
// 	// The junk email folder.
// 	FolderJunk wellKnownFolder = "junkemail"
// 	// The folder that contains items that exist on the local client but could not be uploaded to the server.
// 	FolderLocalFailures wellKnownFolder = "localfailures"
// 	// The "Top of Information Store" folder. This folder is the parent folder for folders that are displayed in normal
// 	// mail clients, such as the inbox.
// 	FolderMsgFolderRoot wellKnownFolder = "msgfolderroot"
// 	// The outbox folder.
// 	FolderOutbox wellKnownFolder = "outbox"
// 	// The folder that contains soft-deleted items: deleted either from the Deleted Items folder, or by pressing
// 	// shift+delete in Outlook. This folder is not visible in any Outlook email client, but end users can interact with
// 	// it through the Recover Deleted Items from Server feature in Outlook or Outlook on the web.
// 	FolderRecoverableItemsDeletions wellKnownFolder = "recoverableitemsdeletions"
// 	// The folder that contains messages that are scheduled to reappear in the inbox using the Schedule feature in
// 	// Outlook for iOS.
// 	FolderScheduled wellKnownFolder = "scheduled"
// 	// The parent folder for all search folders defined in the user's mailbox.
// 	FolderSearchFolders wellKnownFolder = "searchfolders"
// 	// The sent items folder.
// 	FolderSentItems wellKnownFolder = "sentitems"
// 	// The folder that contains items that exist on the server but could not be synchronized to the local client.
// 	FolderServerFailures wellKnownFolder = "serverfailures"
// 	// The folder that contains synchronization logs created by Outlook.
// 	FolderSyncIssues wellKnownFolder = "syncissues"
// )

type Folder struct {
	id string
	// Name is the name of the Mail Folder, do not set this option.
	Name         string `json:"-" yaml:"-" jsonschema:"-,title=Mail Folder Name,example=Inbox"`
	parentID     string
	parentPath   string
	parentFolder *Folder
	// Children are the sub-folders of this Mail Folder.
	// Folders will be processed after this folder in alphanumeric order.
	Children map[string]*Folder `json:",omitempty" jsonschema:"title=Child Folders,description=Child folders."`
	// Categories are sets of rules that apply to this folder. Categories will be processed in alphanumeric order.
	Categories  map[string]Category `jsonschema:"title=Rule Categories,required,description=Categories of rules to process on messages in this folder. Categories will be processed in alphanumeric order."`
	hasChildren bool
	// Filter allows you to filter which messages are retrieved from the API.
	Filter Filter `json:",omitempty" jsonschema:"title=Query Filter,description=Filter rules to limit the messages returned from the API."`
	// Debug will output the un-actioned messages to the specified log folder.
	Debug bool `json:",omitempty" jsonschema:"title=Debug Messages,default=False,description=Save un-actioned messages to a file in the defined log folder."`
}

func (f *Folder) Validate(log *zap.Logger) bool {
	log.Debug("Validating Folder")
	if f == nil {
		return true
	}
	ok := true

	if f.Categories == nil {
		log.Named("Categories").Error("Undefined")
		ok = false
	} else {
		for name, category := range f.Categories {
			category.name = name
			if !category.Validate(log.Named("Category").With(zap.String("Category", category.name))) {
				ok = false
			}
		}
	}
	if f.Children != nil {
		for name, folder := range f.Children {
			folder.Name = name
			folder.parentFolder = f
			folder.parentPath = filepath.Join(f.parentPath, f.Name)
			if !folder.Validate(log.Named("Folder").With(zap.String("Folder", folder.Name))) {
				ok = false
			}
		}
	}

	return ok
}

func (f *Folder) ValidateIDs(client *msgraphsdk.GraphServiceClient, log *zap.Logger) bool {
	if f.id != "" {
		return true
	}

	if f.parentID != "" {
		childFolders, err := client.Me().MailFoldersById(f.parentID).ChildFolders().Get(context.Background(), nil)
		if err != nil {
			msgraph.LogODataError(log, err)

			return false
		}
		for _, folder := range childFolders.GetValue() {
			if *folder.GetDisplayName() != f.Name {
				continue
			}
			f.id = *folder.GetId()
			break
		}
		if f.id == "" {
			log.With(
				zap.String("Path", f.parentPath),
				zap.String("Folder", f.Name),
				zap.Any("Children", childFolders.GetValue()),
			).Error("Folder does not exist!")
			return false
		}
	} else {
		mailFolder, err := client.Me().MailFoldersById(f.Name).Get(context.Background(), nil)
		if err != nil {
			msgraph.LogODataError(log, err)

			return false
		}
		f.id = *mailFolder.GetId()
	}

	if f.Children != nil {
		f.hasChildren = true
		for _, childFolder := range f.Children {
			childFolder.parentID = f.id
			childFolder.ValidateIDs(client, log.Named(childFolder.Name))
		}
	}

	return true
}

func (f *Folder) Handle(client *msgraphsdk.GraphServiceClient, log *zap.Logger,
	debugLog *zap.Logger, queryLimit *int32, logPath string) {
	if f.Categories == nil {
		log.Error("No categories defined!")
		return
	}
	bar := progressbar.NewOptions(
		-1,
		progressbar.OptionSetDescription("Fetching messages from API"),
		progressbar.OptionSetWriter(os.Stderr),
		progressbar.OptionFullWidth(),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionSetPredictTime(true),
		progressbar.OptionShowCount(),
		progressbar.OptionShowIts(),
		progressbar.OptionShowElapsedTimeOnFinish(),
		progressbar.OptionSetItsString("Msgs"),
		progressbar.OptionSpinnerType(14),
		progressbar.OptionSetRenderBlankState(true),
	)

	folderMessages := f.getMessages(client, log.Named("Messages"), queryLimit)
	msgCount := *folderMessages.GetOdataCount()
	log.With(zap.Int64("Count", msgCount)).Info("Message Count")
	bar.ChangeMax64(msgCount)

	// Use PageIterator to iterate through all users
	pageIterator, err := msgraphcore.NewPageIterator(
		folderMessages,
		client.GetAdapter(),
		graphmodels.CreateMessageCollectionResponseFromDiscriminatorValue)
	if err != nil {
		msgraph.LogODataError(log, err)
		if err := bar.Close(); err != nil {
			log.With(zap.Error(err)).Error("Failed to close progress bar.")
		}

		return
	}

	var emails []*message.Message
	var msgIndex int32 = 0
	err = pageIterator.Iterate(context.Background(), func(pageItem interface{}) bool {
		msg := pageItem.(graphmodels.Messageable)
		atomic.AddInt32(&msgIndex, 1)
		if err := bar.Add(1); err != nil {
			log.With(zap.Error(err)).Error("Failed to increment progress bar!")
		}

		emails = append(emails, message.NewMessage(msg))

		if queryLimit != nil && *queryLimit == msgIndex {
			return false
		}

		return true
	})
	if err != nil {
		msgraph.LogODataError(log, err)

		return
	}
	if err := bar.Close(); err != nil {
		log.With(zap.Error(err)).Error("Failed to close progress bar.")
	}

	// Get keys from the map so we can call categories in alphanumeric order
	keys := make([]string, 0, len(f.Categories))
	for k := range f.Categories {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, categoryName := range keys {
		category := f.Categories[categoryName]
		if category.Disable {
			log.Named(categoryName).Warn("Category Disabled")
			continue
		}
		category.name = categoryName
		category.runRules(client, log.Named("Category").Named(categoryName), emails, debugLog.Named(categoryName))
	}

	f.debugMessages(logPath, log.Named("DebugConfig"), emails)

	if f.Children == nil {
		return
	}

	for name, folder := range f.Children {
		folder.Handle(client, log.Named(name), debugLog.Named(name), queryLimit, logPath)
	}
}

func (f *Folder) getMessages(client *msgraphsdk.GraphServiceClient,
	log *zap.Logger, queryLimit *int32) graphmodels.MessageCollectionResponseable {

	queryFilter := f.Filter.buildFilter()
	log.With(zap.String("Filter", queryFilter)).Debug("Setting query filter")

	boolTrue := true

	queryOptions := &messages.MessagesRequestBuilderGetRequestConfiguration{
		QueryParameters: &messages.MessagesRequestBuilderGetQueryParameters{
			Count:   &boolTrue,
			Filter:  &queryFilter,
			Orderby: []string{"receivedDateTime"},
			Search:  nil,
			Select: []string{
				"ccRecipients",
				"createdDateTime",
				"flag",
				"from",
				"hasAttachments",
				"isRead",
				"receivedDateTime",
				"sender",
				"sentDateTime",
				"subject",
				"toRecipients",
				"parentFolderId",
			},
			Skip: nil,
			Top:  queryLimit,
		},
	}
	log.Debug("Fetching Messages")
	emails, err := client.Me().MailFoldersById(f.id).Messages().Get(context.Background(), queryOptions)
	if err != nil {
		msgraph.LogODataError(log, err)

		return nil
	}

	return emails
}

func (f *Folder) debugMessages(logPath string, log *zap.Logger, emails []*message.Message) {
	if !f.Debug {
		return
	}

	fileName := strings.Builder{}
	fileName.WriteString("Debug_Emails_")
	if f.parentPath != "" {
		fileName.WriteString(f.parentPath)
		fileName.WriteString("/")
	}
	fileName.WriteString(f.Name)
	fileName.WriteString(".json")
	filePath := filepath.Join(logPath, strings.ReplaceAll(fileName.String(), "/", "."))
	file, err := os.OpenFile(filePath, os.O_CREATE, os.ModePerm)
	if err != nil {
		log.With(
			zap.Error(err),
			zap.String("File Path", filePath),
		).Error("Failed to open file for writing")
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.With(
				zap.Error(err),
				zap.String("File Path", filePath),
			).Error("Failed to close debug log.")
		}
	}()

	encoder := json.NewEncoder(file)
	for _, email := range emails {
		if email.Actioned {
			continue
		}

		if err := encoder.Encode(email); err != nil {
			log.With(
				zap.Error(err),
				zap.Any("Email", email),
			).Error("Failed to log debug email")
		}
	}
}
