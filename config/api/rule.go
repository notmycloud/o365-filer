package api

import (
	"errors"

	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"gitlab.com/notmycloud/o365-filer/config/actions"
	"gitlab.com/notmycloud/o365-filer/config/engines"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type Rule struct {
	name string
	// Disable will prevent this rule from processing.
	Disable bool `json:",omitempty" jsonschema:"title=Disable Rule,default=False,description=Prevent this rule from processing."`
	// Debug will enable debug logging for this rule only.
	Debug bool `json:",omitempty" jsonschema:"title=Debug log rule,default=False,description=Enable DEBUG level logging for this rule."`
	// Engine is the Rule Engine to match against the messages.
	Engine engines.Engines `jsonschema:"title=Rule Engines,required,description=Rule Engines to process against the messages."`
	// Action is what should happen to the message if matched.
	Action actions.Action `jsonschema:"title=Rule Actions,description=Action(s) to apply if all rules match."`
}

func (r *Rule) Validate(log *zap.Logger) bool {
	log.Debug("Validating Rule")
	if r == nil {
		return true
	}
	ok := true

	if r.Disable {
		log.Info("Disabled")
		return true
	}
	if !r.Engine.Validate(log.Named("Engine")) {
		ok = false
	}
	if !r.Action.Validate(log.Named("Action")) {
		ok = false
	}

	return ok
}

func (r *Rule) Process(client *msgraphsdk.GraphServiceClient, message *message.Message, log *zap.Logger) (bool,
	error) {
	if r.Disable {
		log.Info("Disabled")
		return false, nil
	}

	matched, ok := r.Engine.Match(message, log.Named("Engine"))
	if !ok {
		return false, errors.New("error running match engine")
	}

	if !matched {
		return false, nil
	}

	log.With(
		zap.String("Subject", message.Subject),
		zap.Timep("Received", message.Received),
		zap.String("From", message.From.Address),
		zap.Strings("To", message.GetToArray()),
		zap.Strings("CC", message.GetCCArray()),
		zap.Bool("Attachments", message.HasAttachments),
		zap.Bool("Read", message.IsRead),
		zap.String("Flag", message.Flag),
	).Debug("Message Matched!")

	// Perform action
	if err := r.Action.Perform(client, message, log.Named("Action")); err != nil {
		return false, err
	}

	return true, nil
}
