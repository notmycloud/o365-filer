package config

import (
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type rotate struct {
	// Size will rotate the log files once they reach the specified size in Megabytes.
	Size int `json:",omitempty" jsonschema:"title=Rotate Size (MB),anyof_required=size,example=5,default=0,description=Rotate the log files once they reach the specified size in Megabytes."`
	// Keep will specify how many files to keep.
	Keep int `json:",omitempty" jsonschema:"title=Rotate Retention,anyof_required=keep,example=4,default=0,description=Specify how many rotations to keep."`
	// Age will rotate the log files once they are the specified number of days old.
	Age int `json:",omitempty" jsonschema:"title=Rotate Age,anyof_required=age,example=7,default=0,description=Age in days to trigger a rotation."`
}

func (r *rotate) rotateWriter(fileName string) zapcore.WriteSyncer {
	// lumberjack.Logger is already safe for concurrent use, so we don't need to
	// lock it.
	return zapcore.AddSync(&lumberjack.Logger{
		Filename:   fileName,
		MaxSize:    r.Size, // megabytes
		MaxBackups: r.Keep,
		MaxAge:     r.Age, // days
	})
}
