package actions

import (
	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type Action struct {
	// Move the message to the specified folder.
	Move *actionMove `json:",omitempty" jsonschema:"title=Move Message,anyof_required=move,description=Move the matched message to the specified folder."`
	// Delete the message.
	Delete *actionDelete `json:",omitempty" jsonschema:"title=Delete Message,anyof_required=delete,description=Delete the matched message."`
}

func (a *Action) Validate(log *zap.Logger) bool {
	log.Debug("Validating Actions")
	if a == nil {
		return true
	}
	ok := true

	if !a.Move.Validate(log.Named("Move")) {
		ok = false
	}

	if !a.Delete.Validate(log.Named("Delete")) {
		ok = false
	}

	return ok
}

func (a *Action) Perform(client *msgraphsdk.GraphServiceClient, message *message.Message, log *zap.Logger) error {
	if a.Move != nil {
		if err := a.Move.Move(client, log.Named("Move"), message); err != nil {
			return err
		}

		message.Actioned = true
		return nil
	}
	if a.Delete != nil {
		if err := a.Delete.delete(client, log.Named("Delete"), message); err != nil {
			return err
		}

		message.Actioned = true
		return nil
	}

	log.Warn("No action defined")
	return nil
}
