package actions

import (
	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type actionDelete struct {
	// Confirm deletion as a safeguard.
	Confirm bool `jsonschema:"title=Confirm Delete,required,default=False,description=Confirm the deletion of the matched message."`
	// DryRun will not delete the message but log that it would have been.
	DryRun bool `json:",omitempty" jsonschema:"title=Dry Run,default=False,description=Log that the message would have been deleted without actually doing so."`
}

func (ad *actionDelete) Validate(log *zap.Logger) bool {
	log.Debug("Validating Delete Action")
	if ad == nil {
		return true
	}
	// ok := true

	if ad.DryRun {
		log.Info("DryRun")
	}

	if !ad.Confirm {
		log.Warn("Delete Unconfirmed!")
	}

	return true
}

func (ad *actionDelete) delete(_ *msgraphsdk.GraphServiceClient, log *zap.Logger, message *message.Message) error {
	if ad.DryRun {
		log.With(zap.String("Message", message.Subject)).Warn("DRY RUN: Deleting Message")
		return nil
	}

	// TODO: Delete message
	return nil
}
