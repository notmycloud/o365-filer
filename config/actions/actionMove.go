package actions

import (
	"context"
	"errors"
	"fmt"
	"strings"

	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"github.com/microsoftgraph/msgraph-sdk-go/me/mailfolders"
	"github.com/microsoftgraph/msgraph-sdk-go/me/mailfolders/item/childfolders"
	"github.com/microsoftgraph/msgraph-sdk-go/me/messages/item/move"
	graphmodels "github.com/microsoftgraph/msgraph-sdk-go/models"
	"gitlab.com/notmycloud/o365-filer/app/msgraph"
	"gitlab.com/notmycloud/o365-filer/config/message"
	"go.uber.org/zap"
)

type actionMove struct {
	// DryRun will not move the message, but will log that it would have been.
	DryRun bool `json:",omitempty" jsonschema:"title=Dry Run,default=False,description=Log that the message would have been moved without actually doing so."`
	// Path to move the message to.
	Path string `jsonschema:"title=Destination Path,required,example=Inbox/Vendor/Amazon,description=Path of the Mail Folder to move the message to. Set Create to true if this folder may not exist."`
	// Create the destination folder(s) if they do not already exist,
	// Rule will fail if the destination does not exist and this is set to false.
	Create bool `json:",omitempty" jsonschema:"title=Create Destination,default=False,description=Create the Mail Folder if it and the parent folders do not yet exist."`
}

func (am *actionMove) Validate(log *zap.Logger) bool {
	log.Debug("Validating Move Action")
	if am == nil {
		return true
	}
	ok := true

	if am.DryRun {
		log.Info("DryRun")
	}

	if am.Path == "" {
		log.Error("Path Undefined")
		ok = false
	}

	return ok
}

func (am *actionMove) Move(client *msgraphsdk.GraphServiceClient, log *zap.Logger, message *message.Message) error {
	if am.DryRun {
		log.With(
			zap.String("Message", message.Subject),
			zap.String("Destination", am.Path),
		).Warn("DRY RUN: Moving Message")

		return nil
	}

	parentID := ""
	folderLog := log.Named("Check")

	for _, folder := range strings.Split(am.Path, "/") {
		folderLog = folderLog.Named(folder)
		queryFilter := fmt.Sprintf("displayName eq '%s'", folder)

		// On first iteration, get the root folder ID
		if parentID == "" {
			parentID = am.checkRootFolder(client, folderLog, folder, queryFilter)
			if parentID == "" {
				return errors.New("root folder failed")
			}

			continue
		}

		parentID = am.checkChildFolder(client, folderLog, folder, queryFilter, parentID)
		if parentID == "" {
			return errors.New("child folder failed")
		}
	}

	requestBody := move.NewMovePostRequestBody()
	requestBody.SetDestinationId(&parentID)

	log.With(zap.String("Destination", am.Path)).Warn("Moving Message")
	_, err := client.Me().MessagesById(message.GetID()).Move().Post(context.Background(), requestBody, nil)
	if err != nil {
		msgraph.LogODataError(log, err)

		return errors.New("move query failed")
	}

	return nil
}

func (am *actionMove) checkRootFolder(client *msgraphsdk.GraphServiceClient, log *zap.Logger, folder string,
	filter string) string {
	log.With(zap.String("Folder", folder)).Debug("Looking for Root folder")

	options := &mailfolders.MailFoldersRequestBuilderGetRequestConfiguration{
		QueryParameters: &mailfolders.MailFoldersRequestBuilderGetQueryParameters{
			Filter: &filter,
		},
	}

	rootFolders, err := client.Me().MailFolders().Get(context.Background(), options)
	if err != nil {
		msgraph.LogODataError(log, err)

		return ""
	}

	return am.GetOrCreateFolder(client, rootFolders, log.Named("Get"), "", folder)
}

func (am *actionMove) checkChildFolder(client *msgraphsdk.GraphServiceClient, log *zap.Logger, folder string,
	filter string, parentID string) string {
	// log.With(zap.String("Folder", folder)).Debug("Looking for Child folder")

	options := &childfolders.ChildFoldersRequestBuilderGetRequestConfiguration{
		QueryParameters: &childfolders.ChildFoldersRequestBuilderGetQueryParameters{
			Filter: &filter,
		},
	}

	childFolders, err := client.Me().MailFoldersById(parentID).ChildFolders().Get(context.Background(), options)
	if err != nil {
		msgraph.LogODataError(log, err)

		return ""
	}

	return am.GetOrCreateFolder(client, childFolders, log.Named("Get"), parentID, folder)
}

func (am *actionMove) GetOrCreateFolder(client *msgraphsdk.GraphServiceClient,
	mailFolder graphmodels.MailFolderCollectionResponseable, log *zap.Logger, parentID string, searchFolder string) string {

	for _, folder := range mailFolder.GetValue() {
		log.With(
			zap.String("Criteria", searchFolder),
			zap.Stringp("DisplayName", folder.GetDisplayName()),
		).Debug("Checking available folders")
		if strings.ToLower(*folder.GetDisplayName()) == strings.ToLower(searchFolder) {
			return *folder.GetId()
		}
	}

	// TODO: Handle pagination
	// mailFolder.GetOdataNextLink()
	// // Initialize iterator
	// pageIterator, err := msgraphcore.NewPageIterator(mailFolder, adapter,
	// 	graphmodels.CreateMailFolderFromDiscriminatorValue)
	//
	// // Any custom headers sent in original request should also be added
	// // to the iterator
	// pageIterator.SetHeaders(options.Headers)
	//
	// // Iterate over all pages
	// iterateErr := pageIterator.Iterate(func(pageItem interface{}) bool {
	// 	message := pageItem.(models.Messageable)
	// 	fmt.Printf("%s\n", *message.GetSubject())
	// 	// Return true to continue the iteration
	// 	return true
	// })

	log.Info("Folder does not exist.")
	if !am.Create {
		log.Error("Creation disabled!")
		return ""
	}

	newFolder := am.CreateFolder(client, log.Named("Create"), parentID, searchFolder)
	if newFolder == nil {
		return ""
	}

	return *newFolder.GetId()
}

func (am *actionMove) CreateFolder(client *msgraphsdk.GraphServiceClient, log *zap.Logger, parentID string,
	folderName string) graphmodels.MailFolderable {
	requestBody := graphmodels.NewMailFolder()
	requestBody.SetDisplayName(&folderName)
	isHidden := false
	requestBody.SetIsHidden(&isHidden)

	var mailFolder graphmodels.MailFolderable
	var err error

	if parentID == "" {
		mailFolder, err = client.Me().MailFolders().Post(context.Background(), requestBody, nil)
	} else {
		mailFolder, err = client.Me().MailFoldersById(parentID).ChildFolders().Post(context.Background(), requestBody, nil)
	}

	if err != nil {
		msgraph.LogODataError(log, err)

		return nil
	}

	log.Info("Created folder")
	return mailFolder
}
