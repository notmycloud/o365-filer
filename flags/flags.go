package flags

const (
	TenantID    = "tenantid"
	ClientID    = "clientid"
	Development = "dev"
	LogPath     = "log-path"
	// LogStdOut       = "log-stdout"
	// LogStdErr       = "log-stderr"
	LogDebug        = "log-debug"
	LogFileJSON     = "log-file-json"
	LogConsoleJSON  = "log-console-json"
	LogRotateEnable = "log-rotate-enable"
	LogRotateSize   = "log-rotate-size"
	LogRotateKeep   = "log-rotate-keep"
	LogRotateAge    = "log-rotate-age"
)
