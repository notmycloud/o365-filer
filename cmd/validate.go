/*
Package cmd
Copyright © 2022 Not My Cloud devops@notmy.cloud

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/notmycloud/o365-filer/config"
	"go.uber.org/zap"
)

// schemaCmd represents the schema command
var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "Validate your configuration against the schema",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := config.ValidateSchema(); err != nil {
			return fmt.Errorf("failed to validate config file against schema: %w", err)
		}
		log.Printf("Configuration passes the JSON Schema Validation.\n")

		appConfig, err := config.UnmarshalViper()
		if err != nil {
			return fmt.Errorf("failed to decode config: %w", err)
		}

		if !appConfig.Validate(zap.S().Desugar()) {
			return fmt.Errorf("failed to validate config programatically")
		}
		log.Printf("Configuration passes the Programability Validation.\n")

		log.Printf("Configuration is valid!\n")
		return nil
	},
}

func init() {
	rootCmd.AddCommand(validateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// schemaCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// schemaCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
