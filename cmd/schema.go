/*
Package cmd
Copyright © 2022 Not My Cloud devops@notmy.cloud

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/invopop/jsonschema"
	"github.com/spf13/cobra"
	"gitlab.com/notmycloud/o365-filer/config"
)

// schemaCmd represents the schema command
var schemaCmd = &cobra.Command{
	Use:   "schema",
	Short: "Generate a JSON Schema to validate the config file",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		log.Printf("Creating JSON Schema for the config...")
		r := new(jsonschema.Reflector)
		// if err := r.AddGoComments("gitlab.com/notmycloud/o365-filer", "./"); err != nil {
		// 	return fmt.Errorf("failed to add field comments: %w", err)
		// }

		// Only tags explicitly marked as required instead of any that don't have `json:,omitempty`.
		r.RequiredFromJSONSchemaTags = true

		schema := r.Reflect(&config.Config{})
		data, err := json.MarshalIndent(schema, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to compile JSON Schema: %w", err)
		}

		f, _ := os.OpenFile("./config.schema.json", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
		defer func() {
			if err := f.Close(); err != nil {
				log.Fatalf("Failed to close schema file: %v\n", err)
			}
		}()
		if _, err := f.WriteString(string(data)); err != nil {
			return fmt.Errorf("failed to write schema file: %w", err)
		}

		log.Printf("JSON Schema generated!\n")
		return nil
	},
}

func init() {
	rootCmd.AddCommand(schemaCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// schemaCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// schemaCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
