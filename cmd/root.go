/*
Package cmd
Copyright © 2022 Not My Cloud devops@notmy.cloud

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/notmycloud/o365-filer/app"
	"gitlab.com/notmycloud/o365-filer/config"
	"gitlab.com/notmycloud/o365-filer/flags"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "o365-filer",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	RunE: app.Appcmd,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// if _, err := os.Stat(".env"); err == nil {
	// 	log.Printf("Loading .env\n")
	//
	// 	if err := godotenv.Load(".env"); err != nil {
	// 		log.Fatalf("Failed to load DOTENV. Err: %v", err)
	// 	}
	// } else if errors.Is(err, os.ErrNotExist) {
	// 	log.Printf("DOTNEV not found, continuing...\n")
	// } else {
	// 	// Schrodinger: file may or may not exist. See err for details.
	// 	// Therefore, do *NOT* use !os.IsNotExist(err) to test for file existence
	// 	log.Printf("Failed to determine if DOTENV file exists: %v", err)
	// }
	//
	// log.Printf("OS Environment:\n%s\n", os.Environ())

	cobra.OnInitialize(initConfig)
	binaryName := strings.TrimSuffix(strings.ToUpper(filepath.Base(os.Args[0])), ".EXE")

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		"config file (searches in /etc/"+binaryName+", $HOME/.config/"+binaryName+", and the local dir)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().StringP(flags.TenantID, "t", "", "Your Tenant ID (https://learn.microsoft."+
		"com/en-us/graph/auth-register-app-v2)")
	rootCmd.Flags().StringP(flags.ClientID, "c", "", "Your Client ID (https://learn.microsoft."+
		"com/en-us/graph/auth-register-app-v2)")
	rootCmd.Flags().Bool(flags.Development, false, "Enable Development mode.")
	rootCmd.Flags().StringP(flags.LogPath, "l", "", "Parent directory to save logs.")
	// rootCmd.Flags().Bool(flags.LogStdOut, true, "Log to standard out, in addition to --logpath")
	// rootCmd.Flags().Bool(flags.LogStdErr, false, "Log to standard out, in addition to --logpath")
	rootCmd.Flags().Bool(flags.LogDebug, false, "Enable debug logging.")
	rootCmd.Flags().Bool(flags.LogConsoleJSON, false, "Log to Console in JSON format.")
	rootCmd.Flags().Bool(flags.LogFileJSON, false, "Log to File in JSON format, requires flag --"+flags.LogPath+".")
	rootCmd.Flags().Bool(flags.LogRotateEnable, true, "Enable log rotation.")
	rootCmd.Flags().Int(flags.LogRotateSize, 500, "Max size in Megabytes of log file before rotation.") //nolint:gomnd
	rootCmd.Flags().Int(flags.LogRotateKeep, 3, "Number of rotated logs to keep.")                      //nolint:gomnd
	rootCmd.Flags().Int(flags.LogRotateAge, 7, "Max age (days) of a log file.")                         //nolint:gomnd
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	binaryName := strings.TrimSuffix(strings.ToUpper(filepath.Base(os.Args[0])), ".EXE")

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search for config in /etc, $HOME/.config, and local dir.
		viper.AddConfigPath(filepath.Join("etc", binaryName))
		viper.AddConfigPath(filepath.Join(home, ".config", binaryName))
		viper.AddConfigPath(".")

		// Search for config named config.yaml
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix(binaryName)
	log.Printf("Environment Variables should be prefixed with %s_", strings.ToUpper(binaryName))
	viper.SetEnvKeyReplacer(strings.NewReplacer("_", "-"))

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Printf("Using config file: %s", viper.ConfigFileUsed())
	}

	if viper.GetBool("DebugConfig") {
		viper.Debug()
	}

	if err := config.ValidateSchema(); err != nil {
		panic(fmt.Errorf("failed to validate the config file: %w", err))
	}
}
